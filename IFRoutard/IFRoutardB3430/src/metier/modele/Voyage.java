
package metier.modele;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class Voyage implements Serializable
{
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    protected String nom;
    protected int duree;
    protected String description;
    protected String code;
    protected boolean sejour;
    @OneToMany
    protected List<DateEtCouts> dec = new ArrayList();
    @ManyToOne
    protected Pays pays;
    @OneToMany
    protected List<Devis> listeDevis = new ArrayList();
    
    protected Voyage ()
    {
        
    }
    
    protected Voyage(String nom, int duree, String description, String code)
    {
        this.nom=nom;
        this.code=code;
        this.duree=duree;
        this.description=description;
        //this.unPays = pays;        
    }

    public long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public int getDuree() {
        return duree;
    }

    public String getDescription() {
        return description;
    }

    public String getCode() {
        return code;
    }

    public boolean isSejour() {
        return sejour;
    }

    public List<DateEtCouts> getDec() {
        return dec;
    }

    public List<Devis> getListeDevis() {
        return listeDevis;
    }
    
    public Pays getPays() {
        return pays;
    }
    
    public void addDevis(Devis devis)
    {
        this.listeDevis.add(devis);
    }
    
    public void rmDevis (Devis devis)
    {
        this.listeDevis.remove (devis);
    }
    
    public void addDEC (DateEtCouts dec)
    {
        this.dec.add (dec);
    }
    
    public void rmDEC (DateEtCouts dec)
    {
        this.dec.remove(dec);
    }
    
    public void setPays (Pays pays)
    {
        this.pays=pays;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setSejour(boolean sejour) {
        this.sejour = sejour;
    }
    
}
