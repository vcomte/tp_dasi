
package metier.modele;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;


@Entity
public class DateEtCouts implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateSejour;
    private String lieu;
    private int prix;
    private String transport;
    private String code;
    @ManyToOne
    private Voyage voyage;
    @OneToMany
    private List <Devis> listeDevis = new ArrayList();
    
    public DateEtCouts()
    {
        
    }
 
    public DateEtCouts(Date date, String lieu, int prix, String transport, String code)
    {
        this.dateSejour = date;
        this.code = code;
        this.lieu=lieu;
        this.prix=prix;
        this.transport=transport;
              
    }

    public long getId() {
        return id;
    }

    public Date getDateSejour() {
        return dateSejour;
    }

    public String getLieu() {
        return lieu;
    }

    public int getPrix() {
        return prix;
    }

    public String getTransport() {
        return transport;
    }

    public String getCode() {
        return code;
    }

    public Voyage getVoyage() {
        return voyage;
    }

    public List<Devis> getListeDevis() {
        return listeDevis;
    }
    
    public void addDevis(Devis devis)
    {
        this.listeDevis.add(devis);
    }
    
    public void rmDevis (Devis devis)
    {
        this.listeDevis.remove (devis);
    }
    
    public void setVoyage(Voyage voyage)
    {
        this.voyage = voyage;
    }

    public void setDateSejour(Date dateSejour) {
        this.dateSejour = dateSejour;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    
}
