/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package metier.modele;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

@Entity
public class Conseiller implements Serializable {
    // Attributs
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private String civilite;
    private String name;
    private String surname;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date naissance;
    private String telephone;
    private String adresse;
    private String email;
    @ManyToMany
    private List<Pays> listePays = new ArrayList();
    @OneToMany
    private List<Devis> listeDevis = new ArrayList();
 
    
    public Conseiller() {
    }
    
    public Conseiller(String civilite, String name, String surname, Date naissance,String adresse, String telephone, String email, List<Pays> pays){
        this.civilite=civilite;
        this.name=name;
        this.surname=surname;
        this.naissance=naissance;
        this.adresse=adresse;
        this.email=email;
        this.listePays=pays;
        this.telephone = telephone;
    }
    
    public void addPays(Pays pays)
    {
        this.listePays.add(pays);
        
    }
    
    public void rmPays (Pays pays)
    {
        this.listePays.remove(pays);
    }
    
    public void addDevis(Devis devis)
    {
        this.listeDevis.add(devis);
    }
    
    public void rmDevis (Devis devis)
    {
        this.listeDevis.remove (devis);
    }

    public long getId() {
        return id;
    }
    
    public String getCivilite() {
        return civilite;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
    
    public Date getNaissance() {
        return naissance;
    }
    
    public String getAdresse() {
        return adresse;
    }
    
    public String getTelephone() {
        return telephone;
    }

    public String getEmail() {
        return email;
    }

    public List<Pays> getListePays() {
        return listePays;
    }

    public List<Devis> getListeDevis() {
        return listeDevis;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
