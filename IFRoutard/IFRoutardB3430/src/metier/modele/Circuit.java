package metier.modele;

import java.io.Serializable;
import javax.persistence.Entity;

@Entity
public class Circuit extends Voyage implements Serializable {
    
    
    private String transportation;
    private int kilometers;
    
    public Circuit(){
        
    }
    
    public Circuit(String nom, int duree, String description, String code, String transportation, int kilometers){
        
        super(nom, duree, description, code);
        this.transportation=transportation;
        this.kilometers=kilometers;
        sejour = false;       
    }

    public String getTransportation() {
        return transportation;
    }

    public int getKilometers() {
        return kilometers;
    }

    public void setTransportation(String transportation) {
        this.transportation = transportation;
    }

    public void setKilometers(int kilometers) {
        this.kilometers = kilometers;
    }
}