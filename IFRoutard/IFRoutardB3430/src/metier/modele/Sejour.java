package metier.modele;

import java.io.Serializable;
import javax.persistence.Entity;

@Entity
public class Sejour extends Voyage implements Serializable {
    
    
    private String residence;
    
    public Sejour(){
        
    }
    
    public Sejour(String nom, int duree, String description, String code, String residence){
        super(nom, duree, description, code);
        this.residence=residence;
        sejour = true;
    }

    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }
}
