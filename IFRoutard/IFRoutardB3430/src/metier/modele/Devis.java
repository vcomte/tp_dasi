/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package metier.modele;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Devis implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private int nbPersonnes;
    private int prix;
    @ManyToOne
    private Client client;
    @ManyToOne
    private Conseiller conseiller;
    @ManyToOne
    private Voyage voyage;
    @ManyToOne
    private DateEtCouts dateetcouts;
    
    public Devis(){
        
    }
    
    public Devis(int nbPersonnes, int prix, Client client, Conseiller conseiller, Voyage voyage, DateEtCouts dateetcouts){
        
        this.nbPersonnes=nbPersonnes;
        this.prix=prix;
        this.client=client;
        this.conseiller=conseiller;
        this.voyage=voyage;
        this.dateetcouts=dateetcouts;
          
    }

    public long getId() {
        return id;
    }

    public int getNbPersonnes() {
        return nbPersonnes;
    }

    public int getPrix() {
        return prix;
    }

    public Client getClient() {
        return client;
    }

    public Conseiller getConseiller() {
        return conseiller;
    }

    public Voyage getVoyage() {
        return voyage;
    }

    public DateEtCouts getDateetcouts() {
        return dateetcouts;
    }

    public void setNbPersonnes(int nbPersonnes) {
        this.nbPersonnes = nbPersonnes;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }
}
