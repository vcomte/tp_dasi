package metier.modele;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

/**
 *
 * @author Karim
 */

@Entity
public class Pays implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private String code;
    private String nom;
    private String region;
    private String capitale;
    private float population;
    private float superficie;
    private String langue;
    private String regimePolitique;
    @ManyToMany
    private List<Conseiller> conseillers = new ArrayList();
    @OneToMany
    private List<Voyage> voyages = new ArrayList();
    
    public Pays()
    {
        
    }
    
    public Pays(String code, String nom, String capitale, float population, float superficie, String langue, String region, String regimePolitique){
        this.code = code;
        this.nom=nom;
        this.capitale=capitale;
        this.population=population;
        this.superficie=superficie;
        this.langue=langue;
        this.region = region;
        this.regimePolitique = regimePolitique;
    }
    
    public long getID() {
        return id;
    }
 
    public String getCode() {
        return code;
    }
    
    public String getNom() {
        return nom;
    }
    
    public String getCapitale() {
        return capitale;
    }
    
    public float getPopulation() {
        return population;
    }
    
    public float getSuperficie() {
        return superficie;
    }
    
    public String getLangue() {
        return langue;
    }
    
    public String getRegion() {
        return region;
    }
    
    public String getRegimePolitique() {
        return regimePolitique;
    }
    
    public List<Conseiller> getConseillers(){
        return conseillers;
    }
    
    public List<Voyage> getVoyages(){
        return voyages;
    }
    
    
    public void addConseiller (Conseiller con)
    {
        this.conseillers.add (con);
    }
    
    public void rmConseiller (Conseiller con)
    {
        this.conseillers.remove(con);
    }
    
    public void addVoyage (Voyage voyage)
    {
        this.voyages.add (voyage);
    }
    
    public void rmVoyage (Voyage voyage)
    {
        this.voyages.remove(voyage);
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setCapitale(String capitale) {
        this.capitale = capitale;
    }

    public void setPopulation(float population) {
        this.population = population;
    }

    public void setSuperficie(float superficie) {
        this.superficie = superficie;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }
    
    public void setRegion(String region) {
        this.region = region;
    }
    
    public void setRegimePolitique(String regimePolitique) {
        this.regimePolitique = regimePolitique;
    }
    
}
