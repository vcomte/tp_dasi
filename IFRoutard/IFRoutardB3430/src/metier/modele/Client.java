/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package metier.modele;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

@Entity
public class Client implements Serializable
{
    // Attributs
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private String gender;
    private String name;
    private String surname;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date birthDate;
    private String address;
    private String email;
    private String telephone;
    private boolean accept;
    private String password;
    @OneToMany
    private List <Devis> listeDevis = new ArrayList();

    // Constructeurs
    public Client()
    {
    }
    
    public Client (String gender, String name, String surname, Date birthDate, String address, String email, String telephone, boolean accept, String password)
    {
        this.gender = gender;
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.address = address;
        this.email = email;
        this.telephone = telephone;
        this.accept = accept;
        this.password = password;
    }
    
    // Méthodes
    // Accesseurs
    public boolean getAccept()
    {
        return accept;
    }
    
    public String getName()
    {
        return name;
    }
    
    public String getAddress()
    {
        return address;
    }
    
    public String getTelephone()
    {
        return telephone;
    }

    public long getId() {
        return id;
    }

    public String getGender() {
        return gender;
    }

    public String getSurname() {
        return surname;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
    
    public List <Devis> getListeDevis()
    {
        return listeDevis;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setAccept(boolean accept) {
        this.accept = accept;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public void addDevis(Devis devis)
    {
        this.listeDevis.add(devis);
    }
    
    public void rmDevis (Devis devis)
    {
        this.listeDevis.remove (devis);
    }
    
}
