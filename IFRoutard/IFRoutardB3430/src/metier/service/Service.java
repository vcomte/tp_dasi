package metier.service;

import dao.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import metier.modele.*;

public class Service
{
    private static final String EMAIL = "ifroutard@B340.dasi";
    
    
    // Services de Client
    public static boolean addClient(Client client)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            DaoClient.persistClient (client);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return true;
        }
    
    public static boolean removeClient(Client client)
        {
            // Suppression des devis liés au client
            List <Devis> devis = client.getListeDevis();
            ListIterator it = devis.listIterator();
            
            while (it.hasNext())
            {
                Devis dev = (Devis)it.next();
                removeDevis (dev);
            }
                   
            // Suppression du client
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            DaoClient.removeClient (client);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return true;
        }
    
    public static boolean editClient (Client client)
    {
        JpaUtil.creerEntityManager();
        JpaUtil.ouvrirTransaction();
            
        DaoClient.mergeClient (client);
            
        JpaUtil.validerTransaction();
        JpaUtil.fermerEntityManager();
            
        return true;
    }
        
        public static Client rechercherClient (long id)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            Client res = DaoClient.rechercheClient (id);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
        
        public static Client rechercherClient (String email)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            Client res = DaoClient.rechercheClient (email);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
        
        public static List <Client> obtenirClients ()
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            List <Client> res = DaoClient.rechercheClient ();
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
        
        
        public static boolean connexion (String mail, String mdp)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            boolean res = DaoClient.connexion (mail, mdp);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
        
        public static List <String> infosClient (Client client)
        {
            List <String> infos = new ArrayList <String> ();
            List <Devis> devis = client.getListeDevis();
            
            ListIterator it = devis.listIterator();
            while (it.hasNext())
            {
                Devis dev = (Devis)it.next();                
                String mail = genererMail (dev);
                infos.add (mail);
            }
            
            return infos;
        }
        
        
        // Services de Conseiller
        public static boolean addConseiller(Conseiller arnaqueur, List <Pays> pays)
        {
            
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            
            DaoConseiller.persistConseiller(arnaqueur);
            
            for(int i=0; i<pays.size (); i++)
            {
                //Pays lePays = DaoPays.recherchePays(pays.get(i).getID());
                pays.get(i).addConseiller(arnaqueur);
                DaoPays.mergePays(pays.get(i));
            }
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return true;
        }
        
        public static boolean removeConseiller (Conseiller arnaqueur)
        {
            // Suppression des devis
            List <Devis> devis = arnaqueur.getListeDevis();
            ListIterator it = devis.listIterator();
            
            while (it.hasNext())
            {
                Devis dev = (Devis)it.next();
                removeDevis (dev);
            }
            
            // Suppression des liens des pays
            List <Pays> pays = arnaqueur.getListePays();
            ListIterator it2 = pays.listIterator();
            
            while (it2.hasNext())
            {
                Pays pa = (Pays)it2.next();
                pa.rmConseiller(arnaqueur);
                DaoPays.mergePays(pa);
            }
            
            // Suppression du conseiller
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            DaoConseiller.removeConseiller (arnaqueur);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return true;
        }
        
        public static boolean editConseiller (Conseiller arnaqueur)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();

            DaoConseiller.mergeConseiller (arnaqueur);

            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();

            return true;
        }
        
        public static Conseiller rechercherConseiller (long id)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            Conseiller res = DaoConseiller.rechercheConseiller(id);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
        
        public static List <Conseiller> obtenirConseiller ()
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            List <Conseiller> res = DaoConseiller.rechercheConseiller();
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
        
        public static Conseiller determinerConseiler (Pays lePays)
        {
            List < Conseiller > conseillers = new ArrayList();
            conseillers = Service.obtenirConseiller();
            List < Conseiller > conseillersConcernes = new ArrayList();
            // On obtient les conseillers qui ont le pays en question dans leur liste
            for(int i =0; i<conseillers.size(); i++) {
                for(int j = 0; j< conseillers.get(i).getListePays().size(); j++) {
                    if(conseillers.get(i).getListePays().get(j).getCode() == lePays.getCode()) {
                        conseillersConcernes.add(conseillers.get(i));
                        break;
                    }
                }
            }

            // On recupere celui qui a le moins de devis
            Conseiller conseillerElu = conseillers.get(0);
            int min = conseillers.get(0).getListeDevis().size();
            for(int i = 1; i< conseillers.size(); i++) {
                if(conseillers.get(i).getListeDevis().size() < min) {
                    min = conseillers.get(i).getListeDevis().size();
                    conseillerElu = conseillers.get(i);
                }
            }
            return conseillerElu;
        }
        
        // Services de DateEtCouts
        public static boolean addDateEtCouts(DateEtCouts dec, Voyage voyage)
        {
            
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            dec.setVoyage(voyage);
            DaoDateEtCouts.persistDateEtCouts(dec);
            voyage.addDEC(dec);
            DaoVoyage.mergeVoyage(voyage);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return true;
        }
        
        public static boolean removeDateEtCouts (DateEtCouts dec)
        {
            // Suppression des devis
            List <Devis> devis = dec.getListeDevis();
            ListIterator it = devis.listIterator();
            
            while (it.hasNext())
            {
                Devis dev = (Devis)it.next();
                removeDevis (dev);
            }
            
            // Suppression du lien avec le voyage
            Voyage voy = dec.getVoyage();
            voy.rmDEC(dec);
            DaoVoyage.mergeVoyage(voy);
            
            // Suppression du DEC
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            DaoDateEtCouts.removeDateEtCouts (dec);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return true;
        }
        
        public static boolean editDateEtCouts (DateEtCouts dec)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();

            DaoDateEtCouts.mergeDateEtCouts (dec);

            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();

            return true;
        }
        
        public static List <DateEtCouts> rechercherDateEtCouts(String code)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            List <DateEtCouts> res = DaoDateEtCouts.rechercheDateEtCouts(code);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
        
        public static List <DateEtCouts> obtenirDateEtCouts ()
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            List <DateEtCouts> res = DaoDateEtCouts.rechercheDateEtCouts();
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }       
        
        public static String genererInfos (DateEtCouts dec)
        {
            String infos = ("Au départ de : " + dec.getLieu() + " le " + dec.getDateSejour() + "\tTarif : " + dec.getPrix() + "€\tTransport " + dec.getTransport());
            return infos;
        }
        
        
        // Services de Devis
        public static String addDevis(Devis devis, Conseiller leConseiller, Voyage leVoyage, DateEtCouts leDec, Client leClient)
        {
            
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            DaoDevis.persistDevis(devis);
            
            //Conseiller leConseiller = DaoConseiller.rechercheConseiller(conseiller.getId());
            leConseiller.addDevis(devis);
            DaoConseiller.mergeConseiller(leConseiller);
            
            //Voyage leVoyage = DaoVoyage.rechercheVoyage(voyage.getId());
            leVoyage.addDevis(devis);
            DaoVoyage.mergeVoyage(leVoyage);
            
            //DateEtCouts leDec = DaoDateEtCouts.rechercheDateEtCouts(dec.getId());
            leDec.addDevis(devis);
            DaoDateEtCouts.mergeDateEtCouts(leDec);
            
            //Client leClient = DaoClient.rechercheClient(client.getId());
            leClient.addDevis(devis);
            DaoClient.mergeClient(leClient);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            //////  MAIL envoyé au client   //////
            return genererMail (devis);
        }
        
        public static boolean removeDevis (Devis devis)
        {
            // Suppression des liens avec les autres OM
            Client pigeon = devis.getClient();
            DateEtCouts dec = devis.getDateetcouts();
            Conseiller con  = devis.getConseiller();
            Voyage vacances = devis.getVoyage();
            
            pigeon.rmDevis(devis);
            dec.rmDevis(devis);
            con.rmDevis(devis);
            vacances.rmDevis(devis);
            
            DaoClient.mergeClient(pigeon);
            DaoDateEtCouts.mergeDateEtCouts(dec);
            DaoConseiller.mergeConseiller(con);
            DaoVoyage.mergeVoyage(vacances);

            // Suppression du devis
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            DaoDevis.removeDevis (devis);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return true;
        }
        
        public static boolean editDevis (Devis devis)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();

            DaoDevis.mergeDevis (devis);

            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();

            return true;
        }
        
        public static Devis rechercherDevis (long id)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            Devis res = DaoDevis.rechercheDevis(id);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
        
        public static List <Devis> obtenirDevis ()
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            List <Devis> res = DaoDevis.rechercheDevis();
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
        
        public static String genererMail (Devis devis)
        {
            Date uDate = new Date();    // Date du jour
            Client leClient = devis.getClient();
            Voyage leVoyage = devis.getVoyage();
            Conseiller leConseiller = devis.getConseiller();
            DateEtCouts leDec = devis.getDateetcouts();
            String mail = "";
            
            mail += ("Date : " + uDate + "\n" +
                    leClient.getSurname() + " " + leClient.getName() + "\n" +
                    leClient.getAddress() + "\n" +
                    leClient.getTelephone() + "\n\n" +
                    
                    "Votre conseiller pour ce voyage : " + leConseiller.getName() + " " + leConseiller.getSurname() + " (" + leConseiller.getEmail() + ")\n\n" +
                    
                    "Votre voyage : " + leVoyage.getNom() + " (Code " + leVoyage.getCode() + "). " + leVoyage.getPays().getNom() + "\n");
            
            if (leVoyage.isSejour())
            {
                Sejour sej = (Sejour)leVoyage;
                mail += ("Séjour (" + sej.getDuree() + " jours, " + sej.getResidence() + ")\n\n" );
            }
            else
            {
                Circuit cir = (Circuit)leVoyage;
                mail += ("Circuit (" + cir.getDuree() + " jours, " + cir.getKilometers() + " km, " + cir.getTransportation() + ")\n\n" );
            }
            
            mail += ("Départ : le " + leDec.getDateSejour() + " de " + leDec.getLieu() + "\n" +
                    "Transport : " + leDec.getTransport() + " (susceptible d'être modifié) \n\n" +
                    
                    leVoyage.getDescription() + "\n\n" +
                    
                    "———————————————————————————————————\n" +
                    "Nombre de personnes : " + devis.getNbPersonnes() + "\n" +
                    "Tarif par personne : " + devis.getPrix() + " €\n" +
                    "TOTAL : " + devis.getPrix() * devis.getNbPersonnes() + " €");
            
            return mail;
        }
                
        
        // Services de PartenaireCommercial
        public static boolean addPartenaireCommercial (PartenaireCommercial pc)
        {
            
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            DaoPartenaireCommercial.persistPartenaireCommercial (pc);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return true;
        }
        
        public static boolean removePartenaireCommercial (PartenaireCommercial pc)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            DaoPartenaireCommercial.removePartenaireCommercial (pc);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return true;
        }
        
        public static boolean editPartenaireCommercial (PartenaireCommercial pc)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();

            DaoPartenaireCommercial.mergePartenaireCommercial (pc);

            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();

            return true;
        }
    
    public static PartenaireCommercial rechercherPartenaireCommercial (long id)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            PartenaireCommercial res = DaoPartenaireCommercial.recherchePartenaireCommercial (id);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
        
        public static List <PartenaireCommercial> obtenirPartenaireCommercial ()
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            List <PartenaireCommercial> res = DaoPartenaireCommercial.recherchePartenaireCommercial ();
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
        
        public static String genererAlerte (PartenaireCommercial pc, Client client)
        {
            String mail = "";
            mail += ("Expéditeur : " + EMAIL + "\n");
            mail += ("Pour : " + pc.getEmail() + "\n");
            mail += ("Sujet : nouveau client"  + "\n");
            mail += ("Corps : \n" + "nom : " + client.getSurname()  + "\n" + "prénom : " + client.getName() + "\n" + "addresse mail : " + client.getEmail());
            
            return mail;
        }
        
        
        // Services de Pays
        public static boolean addPays(Pays pays)
        {
            
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            DaoPays.persistPays(pays);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return true;
        }
        
        public static boolean removePays (Pays pays)
        {
            // Suppression des voyages associés
            List <Voyage> vac = pays.getVoyages();
            ListIterator it = vac.listIterator();
            
            while (it.hasNext())
            {
                Voyage voy = (Voyage)it.next();
                removeVoyage (voy);
            }
            
            // Suppression des liens avec le conseiller
            List <Conseiller> con = pays.getConseillers();
            ListIterator it2 = con.listIterator();
            
            while (it2.hasNext())
            {
                Conseiller tCon = (Conseiller)it2.next();
                tCon.rmPays(pays);
                DaoConseiller.mergeConseiller(tCon);
            }
            
            // Suppression du pays
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            DaoPays.removePays (pays);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return true;
        }
        
        public static boolean editPays (Pays pays)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();

            DaoPays.mergePays (pays);

            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();

            return true;
        }
        
        public static Pays rechercherPays (long id)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            Pays res = DaoPays.recherchePays (id);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
        
        public static Pays rechercherPaysByCode (String code)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            Pays res = DaoPays.recherchePaysByCode(code);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
        
        public static List <Pays> obtenirPays ()
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            List <Pays> res = DaoPays.recherchePays();
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
        
        // Permet d'obtenir tous les voyages par pays dont on transmet l'ID
    public static List <Voyage> rechercherVoyagePays (long id)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            List <Voyage> res = DaoPays.rechercheVoyages(id);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
    
    // Permet d'obtenir tous les séjours ou circuits par pays dont on transmet l'ID
    public static List <Voyage> rechercherVoyagePays (long id, boolean sejour)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            List <Voyage> res = DaoPays.rechercheVoyages (id, sejour);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
    
    
    public static String genererInfos (Pays pays)
    {
        String infos = "————————————————————————————————————————————————————————————————————————————————\n";
        infos += (pays.getNom().toUpperCase() + "\n");
        infos += (pays.getCapitale() + " (cap), " + pays.getPopulation() + " habitants, " +
                pays.getSuperficie() + " km², langue officielle : " + pays.getLangue() + "\n");
        infos += "————————————————————————————————————————————————————————————————————————————————";
        
        return infos;
    }
    
    
    // Services de Voyage
    public static boolean addVoyage (Voyage vacances, Pays pays)
        {
            
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            DaoVoyage.persistVoyage(vacances);
            
            //DateEtCouts leDec = new DateEtCouts(date, lieu, prix, transport, codeDec);
            //leDec.setVoyage(vacances);
            //DaoDateEtCouts.persistDateEtCouts(leDec);
            //vacances.addDEC(leDec);
            
            //Pays lePays = DaoPays.recherchePays(pays.getID());
            vacances.setPays(pays);
            DaoVoyage.mergeVoyage(vacances);
            pays.addVoyage(vacances);
            DaoPays.mergePays(pays);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return true;
        }
    
    public static boolean removeVoyage (Voyage vacances)
        {
            // Suppression des devis
            List <Devis> devis = vacances.getListeDevis();
            ListIterator it = devis.listIterator();
            
            while (it.hasNext())
            {
                Devis dev = (Devis)it.next();
                removeDevis (dev);
            }
            
            // Suppression des DateEtCouts
            List <DateEtCouts> dec = vacances.getDec();
            ListIterator it2 = dec.listIterator();
            
            while (it2.hasNext())
            {
                DateEtCouts d = (DateEtCouts)it2.next();
                removeDateEtCouts(d);
            }
            
            // Suppression des liens avec le pays
            Pays pays = vacances.getPays();
            pays.rmVoyage(vacances);
            DaoPays.mergePays(pays);
            
            // Suppression du voyage
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            DaoVoyage.removeVoyage (vacances);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return true;
        }
    
    public static boolean editVoyage (Voyage vacances)
    {
        JpaUtil.creerEntityManager();
        JpaUtil.ouvrirTransaction();
            
        DaoVoyage.mergeVoyage (vacances);
            
        JpaUtil.validerTransaction();
        JpaUtil.fermerEntityManager();
            
        return true;
    }
    
    public static Voyage rechercherVoyage (String code)
        {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            
            Voyage res = DaoVoyage.rechercheVoyage(code);
            
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();
            
            return res;
        }
    
    // Permet d'obtenir tous les séjours ou tous les circuits
    public static List <Voyage> obtenirVoyages (boolean sejour)
    {
        JpaUtil.creerEntityManager();
        JpaUtil.ouvrirTransaction();
            
        List <Voyage> res = DaoVoyage.rechercheVoyage(sejour);
            
        JpaUtil.validerTransaction();
        JpaUtil.fermerEntityManager();
            
        return res;
    }
        
    public static List <Voyage> obtenirVoyages ()
    {
        JpaUtil.creerEntityManager();
        JpaUtil.ouvrirTransaction();
            
        List <Voyage> res = DaoVoyage.rechercheVoyage();
            
        JpaUtil.validerTransaction();
        JpaUtil.fermerEntityManager();
            
        return res;
    }
    
    
    public static String genererInfos (Voyage voyage)
    {
        String infos = "";
        
        infos += ("[" + voyage.getCode() + "] " + voyage.getNom() + "\n");
        
        if (voyage.isSejour())
        {
            infos += "Séjour, ";
        }
        else
        {
            infos += "Circuit, ";
        }
        
       infos += (voyage.getDuree() + " jours\n");
       infos += (voyage.getDescription() + "\n\n");
       
       infos += "* Fiche voyage\n";
        
        if (voyage.isSejour())
        {
            Sejour sej = (Sejour)voyage;
            infos += ("Résidence : " + sej.getResidence() + "\n\n");
        }
        else
        {
            Circuit cir = (Circuit)voyage;
            infos += ("Transport : " + cir.getTransportation() + "\n" + "Nombre de km parcourus : " + cir.getKilometers() + " km" + "\n\n" );
        }
        
        infos += "* Périodes et tarifs\n";
        List <DateEtCouts> dec = voyage.getDec();
        ListIterator it = dec.listIterator();
        while (it.hasNext())
        {
            DateEtCouts naze = (DateEtCouts) it.next();
            infos += ("Au départ de " + naze.getLieu() + " le " + naze.getDateSejour() + "        Tarif : " + naze.getPrix() + "€     Transport " + naze.getTransport() + "\n");
        }
        
        return infos;
    }
}
