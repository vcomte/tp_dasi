package dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import metier.modele.*;


public class DaoDevis {
    
    public static void persistDevis (Devis devis)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        em.persist(devis);
    }
    
    public static void removeDevis (Devis devis)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.remove(devis);
    }
    
    public static void mergeDevis(Devis devis)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.merge(devis);
    }
    
    public static Devis rechercheDevis (long id)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select c from Devis c where c.id = :id");
            query.setParameter ("id", id);
            Devis res = (Devis) query.getSingleResult();
            return res;
        }
        catch (Exception e)
        {
            
        }
        
        return null;
    }
    
    public static List <Devis> rechercheDevis ()
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select c from Devis c");
            List <Devis> res = (List <Devis>) query.getResultList();
            return res;
        }
        catch (Exception e)
        {
            
        }
        
        return null;
    }
}
