/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import metier.modele.DateEtCouts;
import metier.modele.Voyage;

/**
 *
 * @author jules
 */
public class DaoDateEtCouts
{
    public static void persistDateEtCouts (DateEtCouts dec)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        em.persist(dec);
    }
    
    public static void removeDateEtCouts (DateEtCouts dec)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.remove(dec);
    }
    
    public static void mergeDateEtCouts (DateEtCouts dec)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.merge(dec);
    }
    
    
    public static List <DateEtCouts> rechercheDateEtCouts (String code)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select d from DateEtCouts d where d.code = :code");
            query.setParameter ("code", code);
            List <DateEtCouts> res = (List <DateEtCouts>) query.getResultList();
            return res;
        }
        catch (Exception e)
        {
            
        }
        
        return null;
    }
    
    public static List <DateEtCouts> rechercheDateEtCouts ()
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select d from DateEtCouts d");
            List <DateEtCouts> res = (List <DateEtCouts>) query.getResultList();
            return res;
        }
        catch (Exception e)
        {
            
        }
        
        return null;
    }
}
