/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import metier.modele.Pays;
import metier.modele.Voyage;

/**
 *
 * @author karim
 */
public class DaoPays {
    public static void persistPays (Pays pays)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        em.persist(pays);
    }
    
    public static void removePays (Pays pays)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.remove(pays);
    }
    
    public static void mergePays (Pays pays)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.merge(pays);
    }
    
   
    public static Pays recherchePays (long id)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select p from Pays p where p.id = :id");
            query.setParameter ("id", id);
            return (Pays) query.getSingleResult();
        }
        catch (Exception e)
        {
            
        }
        return null;
    }
    
    public static Pays recherchePaysByCode (String code)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select p from Pays p where p.code = :code");
            query.setParameter ("code", code);
            return (Pays) query.getSingleResult();
        }
        catch (Exception e)
        {
            
        }
        return null;
    }
    
    public static List <Pays> recherchePays ()
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select p from Pays p");
            List <Pays> res = (List <Pays>) query.getResultList();
            return res;
        }
        catch (Exception e)
        {
            
        }
        return null;
    }
    
    public static List <Voyage> rechercheVoyages (long id)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select p.voyages from Pays p where p.id = :id");
            query.setParameter ("id", id);
            List <Voyage> res = (List <Voyage>) query.getResultList();
            return res;
        }
        catch (Exception e)
        {
            
        }
        
        return null;
    }
    
    public static List <Voyage> rechercheVoyages (long id, boolean sejour)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
    
        // Récupération de tous les voyages
        try
        {
            Query query = em.createQuery ("select p.voyages from Pays p where p.id = :id");
            query.setParameter ("id", id);
            List <Voyage> res = (List <Voyage>) query.getResultList();

            // Récupération de ce qui nous intéresse uniquement
            List <Voyage> resFin = new ArrayList ();
            ListIterator it = res.listIterator();

            while (it.hasNext())
            {
                Voyage vacances = (Voyage) it.next();
                if (vacances.isSejour() == sejour)
                {
                    resFin.add (vacances);
                }
            }

            return resFin;
        }
        catch (Exception e)
        {
            
        }
        return null;
    }
}
