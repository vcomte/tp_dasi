/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import metier.modele.PartenaireCommercial;

/**
 *
 * @author jules
 */
public class DaoPartenaireCommercial
{
    public static void persistPartenaireCommercial (PartenaireCommercial pc)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        em.persist(pc);
    }
    
    public static void removePartenaireCommercial (PartenaireCommercial pc)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.remove(pc);
    }
    
    public static void mergePartenaireCommercial (PartenaireCommercial pc)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.merge(pc);
    }
    
    
    public static PartenaireCommercial recherchePartenaireCommercial (long id)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select pc from PartenaireCommercial pc where pc.id = :id");
            query.setParameter ("id", id);
            PartenaireCommercial res = (PartenaireCommercial) query.getSingleResult();
            return res;
        }
        catch (Exception e)
        {
            
        }
        
        return null;
    }
    
    public static List <PartenaireCommercial> recherchePartenaireCommercial ()
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select pc from PartenaireCommercial pc");
            List <PartenaireCommercial> res = (List <PartenaireCommercial>) query.getResultList();
            return res;
        }
        catch (Exception e)
        {
            
        }
        
        return null;
    }
}
