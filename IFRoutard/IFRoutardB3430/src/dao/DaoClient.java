package dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import metier.modele.*;


public class DaoClient {
    
    public static void persistClient (Client client)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.persist(client);
    }
    
    public static void removeClient (Client client)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.remove(client);
    }
    
    public static void mergeClient(Client client)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.merge(client);
    }
    
    public static Client rechercheClient (long id)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select c from Client c where c.id = :id");
            query.setParameter ("id", id);
            Client res = (Client) query.getSingleResult();
            return res;
        }
        catch (Exception e)
        {
            
        }
        return null;
    }
    
    public static Client rechercheClient (String email)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select c from Client c where c.email = :email");
            query.setParameter ("email", email);
            Client res = (Client) query.getSingleResult();
            return res;
        }
        catch (Exception e)
        {
            
        }
        
        return null;
    }
    
    public static List <Client> rechercheClient ()
    {        
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select c from Client c");
            List <Client> res = (List <Client>) query.getResultList();
            return res;
        }
        catch (Exception e)
        {
            
        }
                
        return null;
    }
    
    public static boolean connexion (String email, String mdp)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        Query query = em.createQuery ("select c from Client c where c.email = :email and c.password = :mdp");
        query.setParameter ("email", email);
        query.setParameter ("mdp", mdp);
        
        // On essaye d'obtenir un résultat, avec un try catch car cela cause une erreur s'il n'y en a pas
        try
        {
            Client res = (Client) query.getSingleResult();
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
    
}
