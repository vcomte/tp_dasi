/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import metier.modele.Voyage;

/**
 *
 * @author Administrateur
 */
public class DaoVoyage
{
    public static void persistVoyage (Voyage vacances)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        em.persist(vacances);
    }
    
    public static void removeVoyage (Voyage vacances)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.remove(vacances);
    }
    

    public static void mergeVoyage(Voyage vacances)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.merge(vacances);
    }
    
    public static Voyage rechercheVoyage (String code)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select v from Voyage v where v.code = :code");
            query.setParameter ("code", code);
            Voyage res = (Voyage) query.getSingleResult();
            return res;
        }
        catch (Exception e)
        {
            
        }
        
        return null;
    }
    
    public static List <Voyage> rechercheVoyage (boolean sejour)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select v from Voyage v where v.sejour = :sejour");
            query.setParameter ("sejour", sejour);
            List <Voyage> res = (List <Voyage>) query.getResultList();
            return res;
        }
        catch (Exception e)
        {
            
        }
        
        return null;
    }
    
    public static List <Voyage> rechercheVoyage ()
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select v from Voyage v");
            List <Voyage> res = (List <Voyage>) query.getResultList();
            return res;
        }
        catch (Exception e)
        {
            
        }
        
        return null;
    }
}
