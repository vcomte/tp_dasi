/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import metier.modele.*;


public class DaoConseiller {
    
    public static void persistConseiller(Conseiller arnaqueur)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        em.persist(arnaqueur);
    }
    
    public static void removeConseiller (Conseiller arnaqueur)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.remove(arnaqueur);
    }
    
    public static void mergeConseiller(Conseiller arnaqueur)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.merge(arnaqueur);
    }

    public static Conseiller rechercheConseiller (long id)
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select c from Conseiller c where c.id = :id");
            query.setParameter ("id", id);
            Conseiller res = (Conseiller) query.getSingleResult();
            return res;
        }
        catch (Exception e)
        {
            
        }
        
        return null;
    }
    
    public static List <Conseiller> rechercheConseiller ()
    {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        try
        {
            Query query = em.createQuery ("select c from Conseiller c");
            List <Conseiller> res = (List <Conseiller>) query.getResultList();
            return res;
        }
        catch (Exception e)
        {
            
        }
        
        return null;
    }

}
