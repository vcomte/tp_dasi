/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import metier.modele.*;
import metier.service.Service;
import util.*;

/**
 *
 * @author karim et Jules surtout
 */
public class Console
{
    private static final String ADRESSE = "/Users/jules/Documents/Cours/INSA/Troisième année/DASI/tp_dasi/IFRoutard/IFRoutardB3430/src/vue/data/";
    
    public static Client decoupeString (String s)
    {
        String[] infos = new String[9];
        String temp = "";
        int j = 0;
        
        // Récupération des différentes parties dans une seule chaîne
        for (int i = 0; i < s.length(); i++)
        {
            if (s.charAt(i) == ',' || i == s.length() - 1)
            {
                if (i == s.length() - 1)
                {
                    temp += s.charAt(i);
                }
                infos[j++] = temp;
                temp = "";
            }
            else
            {
                temp += s.charAt(i);
            }
        }
        
        // Création d'un client à partir de ça
        Date date = LectureDonneesCsv.parseDate (infos[3]);
        
        if ("1".equals(infos[7]))
        {
            Client client = new Client (infos[0], infos[1], infos[2], date, infos[4], infos[5], infos[6], true, infos[8]);
            Service.addClient (client);
            return client;
        }
        else
        {
            Client client = new Client (infos[0], infos[1], infos[2], date, infos[4], infos[5], infos[6], false, infos[8]);
            Service.addClient (client);
            return client;
        }
    }
    
    public static void un (String ADRESSE) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        // Ajout PAYS
        LectureDonneesCsv paysCSV = new LectureDonneesCsv(ADRESSE+"IFRoutard-Pays.csv");
        paysCSV.lirePays(-1);
        paysCSV.fermer();
        
        // Ajout VOYAGES SEJOUR
        LectureDonneesCsv sejourCSV = new LectureDonneesCsv(ADRESSE+"IFRoutard-Voyages-Sejours.csv");
        sejourCSV.lireSejour(-1);
        sejourCSV.fermer();

        // Ajout VOYAGES CIRCUIT
        LectureDonneesCsv circuitCSV = new LectureDonneesCsv(ADRESSE+"IFRoutard-Voyages-Circuits.csv");
        circuitCSV.lireCircuit(-1);
        circuitCSV.fermer();
        
        // Ajout DEPARTS
        LectureDonneesCsv departsCSV = new LectureDonneesCsv(ADRESSE+"IFRoutard-Departs.csv");
        departsCSV.lireDec(-1);
        departsCSV.fermer();
        
        // Ajout CONSEILLERS
        LectureDonneesCsv conseillerCSV = new LectureDonneesCsv(ADRESSE+"IFRoutard-Conseillers.csv");
        conseillerCSV.lireConseiller(-1);
        conseillerCSV.fermer();
    }
    
    public static void deux (String ADRESSE) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        // Inscription des clients
        LectureDonneesCsv clientsCSV = new LectureDonneesCsv(ADRESSE+"IFRoutard-Clients.csv");
        clientsCSV.lireClients(7000);
        clientsCSV.fermer();
    }
    
    public static void trois ()
    {
        // Récupération des clients
        List <Client> clients = Service.obtenirClients();
        
        ListIterator it = clients.listIterator();
        while (it.hasNext())
        {
            Client c = (Client)it.next();
            
            // Obtention des infos du client et affichage de ces infos
            List <String> infos = Service.infosClient(c);
            
            ListIterator it2 = infos.listIterator();
            while (it2.hasNext())
            {
                System.out.println ((String)it2.next() + "\n\n");
            }
        }
    }
    
    public static Client quatre ()
    {
        String invite = ("\nVeuillez entrer dans l'ordre, et séparés par des virgules :\n civilité, prénom, nom, date de naissance, adresse, adresse e-mail, numéro de téléphone, "
                + "acceptation d'utilisation des informations personnelles (0/1), mot de passe\n");
        String rep = Saisie.lireChaine(invite);
        
        return decoupeString(rep);
    }
    
    public static Devis cinq (Client client)
    {
        // Affichage des différents voyages possibles et choix parmi ceux-là
        System.out.println ("Veuillez choisir parmi les voyages suivants :\n");
        List <Voyage> voy = Service.obtenirVoyages();
        List <Integer> choix = new ArrayList ();
        
        ListIterator it = voy.listIterator();
        int i = 1;
        while (it.hasNext())
        {
            choix.add (i);
            Voyage vo = (Voyage)it.next();
            System.out.println ("-----> " + i++ + " <----- " + Service.genererInfos(vo) + "\n");
        }
        
        int ind = Saisie.lireInteger("Quel sera votre choix ?\n", choix);
        
        
        // Affichage des DateEtCouts du voyage sélectionné
        System.out.println ("\n\nVeuillez choisir parmi les départs suivants :\n");
        Voyage v = voy.get(ind - 1);
        List <DateEtCouts> dec = v.getDec();
        
        List <Integer> choix2 = new ArrayList ();
        
        ListIterator it2 = dec.listIterator();
        int ii = 1;
        while (it2.hasNext())
        {
            choix2.add (ii);
            DateEtCouts d = (DateEtCouts)it2.next();
            System.out.println ("-----> " + ii++ + " <----- " + Service.genererInfos (d) + "\n");
        }
        
        int cDec = Saisie.lireInteger ("Quel sera votre choix ?\n", choix2);
        DateEtCouts dd = dec.get(cDec - 1);
        
        Conseiller con = Service.determinerConseiler (v.getPays());
        
        // Détermination du nombre de personnes
        int nbPers = Saisie.lireInteger("\nCombien de personnes pour ce voyage ?\n");
        
        // Création du devis (enfin)
        Devis devis = new Devis (nbPers, dd.getPrix(), client, con, v, dd);
        Service.addDevis (devis, con, v, dd, client);
        
        System.out.println ("\n");
        return devis;
    }
    
    public static void six (Devis devis)
    {
        // Affichage du devis
        System.out.println ("\n" + Service.genererMail (devis) + "\n\n");
    }
    
    
    public static void main (String[] args) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        String question = ("Que voulez-vous faire ?\n"
                + "1. Initialisation des voyages, pays, et conseillers\n"
                + "2. Inscription des clients depuis le fichier CSV\n"
                + "3. Affichage des informations de tous les clients inscrits\n"
                + "4. Inscription interactive d'un client\n"
                + "5. Création interactive d'un devis\n"
                + "6. Affichage du devis\n"
                + "7. Je veux sortir !\n\n");
        int choix = 0;
        List <Integer> lChoix = new ArrayList <Integer> ();
        lChoix.add (1);
        lChoix.add (2);
        lChoix.add (3);
        lChoix.add (4);
        lChoix.add (5);
        lChoix.add (6);
        lChoix.add (7);
        Client client = null;
        Devis devis = null;
        
        while (choix != 7)
        {
            choix = Saisie.lireInteger (question, lChoix);
            
            switch (choix)
            {
                case 1 :
                    un (ADRESSE);
                    break;
                case 2 :
                    deux (ADRESSE);
                    break;
                case 3 :
                    trois ();
                    break;
                case 4 :
                    client = quatre ();
                    break;
                case 5 :
                    devis = cinq (client);
                    break;
                case 6 :
                    six (devis);
                    break;
            }
        }
    }
}
