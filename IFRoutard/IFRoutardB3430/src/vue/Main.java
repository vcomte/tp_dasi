/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import metier.modele.*;
import metier.service.*;
import util.*;

/**
 *
 * @author Administrateur
 */
public class Main
{
    
    //public static void main (String[] args)
    //{
        /** Ajouts d'objets métier **/   
        
        // Ajouts de clients
        /*Date date;
        Date date2;
        Date date3;
        date = LectureDonneesCsv.parseDate("1557-04-29");
        date2 = LectureDonneesCsv.parseDate("1993-01-21");
        date3 = LectureDonneesCsv.parseDate("2007-06-22");
        Client pigeon = new Client ("NGUYEN", "Virgile", "Hector", date, "3 rue de rien du tout", "nguyen.nguyen@viet.sex", "0931746821", true, "gandalf");
        Client rapace = new Client ("M.", "Tuan Hoang", "NGUYEN", date2, "Quelque part", "tuan-hoang.nguyen@insa-lyon.fr", "0911223344", true, "coturnetropfortalepee");
        Client voleur = new Client ("Indef", "Karim", "Truc", date3, "Nulle part", "karim.rien@b3430.3if", "0933546821", false, "123456");
        
        Service.addClient(pigeon);
        Service.addClient(rapace);
        Service.addClient(voleur);
        
        //Ajout de pays
        Pays france = new Pays ("CODE", "France", "Paris", 69000000, 3600, "Français", "blabla", "blabla");
        Service.addPays(france);
        
        // Ajout de voyages
        Voyage japon = new Circuit ("Nihon no sakura", 35, "Voyage sympa loin de NGUYEN", "12", "Avion et tortue", 9999);
        Voyage antarctique = new Sejour ("Au pays des manchots", 24, "Petit séjour glacé", "8", "Chez les scientiques vietnamiens");
        
        Service.addVoyage(japon, france);
        Service.addVoyage(antarctique, france);
                       
        //Ajout dates et couts
        DateEtCouts tresCher = new DateEtCouts (date, "Boukistan", 6700, "à la nage : manchot & compagnie", "MOL396");
        DateEtCouts cher = new DateEtCouts (date3, "Jusqu'au bout du monde", 27999, "à dos de poulet : compagnie pour la protection des poules", "3A1");
        
        Service.addDateEtCouts(tresCher, japon);
        Service.addDateEtCouts(cher, antarctique);
        
        //Ajout Conseiller
        List <Pays> paysArnaque = new ArrayList<Pays>();
        Conseiller arnaqueurPro = new Conseiller ("Mr", "Eric", "Cartman", date, "adresse", "phone",  "enculeurdemouche@glandeur.fr",paysArnaque);
        paysArnaque.add(france);
        Service.addConseiller(arnaqueurPro, paysArnaque);
        
        //Ajout de Devis
        Devis dream = new Devis (3, 6700, rapace, arnaqueurPro, japon, japon.getDec().get(0));
        System.out.println (Service.addDevis(dream, arnaqueurPro, japon, japon.getDec().get(0), rapace));
        
        // Ajout de partenaires commerciaux
        PartenaireCommercial nintendo = new PartenaireCommercial ("にんてんどう", "fire.emblem@nintendo.ni");
        PartenaireCommercial fraise = new PartenaireCommercial ("Fraise l'entreprise informatique", "fraise.salade@fruit.com");
        
        Service.addPartenaireCommercial (nintendo);
        Service.addPartenaireCommercial (fraise);
        
        */
        /** Tests de requêtes **/
        
        // Test d'obtention d'un client par son ID
        /*long id = pigeon.getId();
        Client nguyen = Service.rechercherClient(id);
        System.out.println (nguyen.getName() + "\n");
        
        //Test d'ajout d'un devis à un client
        //Service.ajouterDevis(rapace, dream);
        //List <Devis> blabla = rapace.getListeDevis();
        //System.out.println (blabla.get(0).getPrixTotal() + "\n");
        
        // Test d'obtention de tous les clients
        List <Client> nazes = Service.obtenirClients();
        ListIterator it = nazes.listIterator();
        
        while (it.hasNext())
        {
            Client naze = (Client) it.next();
            System.out.println (naze.getId() + ", " + naze.getGender() + ", " + naze.getName() + ", " + naze.getSurname() + ", " +  naze.getBirthDate() + ", " + naze.getAddress() + ", " + naze.getEmail() + ", " + naze.getTelephone() + ", " + naze.getAccept() + ", " +  naze.getPassword());
        }
        
        
        // Test d'obtention d'un voyage par son ID
        long idee = japon.getId();
        Voyage nihon = Service.rechercherVoyage("FDS");
        System.out.println (nihon.getNom() + "\n");
        
        // Test d'obtention de tous les voyages
        List <Voyage> vacances = Service.obtenirVoyages();
        ListIterator it2 = vacances.listIterator();
        
        while (it2.hasNext())
        {
            Voyage vacance = (Voyage) it2.next();
            if (vacance.isSejour())
            {
                Sejour sej = (Sejour) vacance;
                System.out.println (sej.getId() + ", " + sej.getNom() + ", " + sej.getDuree() + ", " + sej.getDescription() + ", " +  sej.getCode() + ", " + sej.getResidence());
            }
            else
            {
                Circuit circ = (Circuit) vacance;
                System.out.println (circ.getId() + ", " + circ.getNom() + ", " + circ.getDuree() + ", " + circ.getDescription() + ", " +  circ.getCode() + ", " + circ.getTransportation() + ", " + circ.getKilometers());
            }
        }
        
        // Test d'obtention de tous les séjours
        List <Voyage> vac = Service.obtenirVoyages(true);
        ListIterator it3 = vac.listIterator();
        
        while (it3.hasNext())
        {
            Voyage vacance = (Voyage) it3.next();
            Sejour sej = (Sejour) vacance;
            System.out.println (sej.getId() + ", " + sej.getNom() + ", " + sej.getDuree() + ", " + sej.getDescription() + ", " +  sej.getCode() + ", " + sej.getResidence());
        }
        
        
        // Test d'obtention d'un partenaire commercial par son ID
        long ideuh = nintendo.getId();
        PartenaireCommercial wiiU = Service.rechercherPartenaireCommercial(ideuh);
        System.out.println (wiiU.getNom() + "\n");
        
        // Test d'obtention de tous les partenaires commerciaux
        List <PartenaireCommercial> spam = Service.obtenirPartenaireCommercial();
        ListIterator it94 = spam.listIterator();
        
        while (it94.hasNext())
        {
            PartenaireCommercial naze = (PartenaireCommercial) it94.next();
            System.out.println (naze.getId() + ", " + naze.getNom() + ", " + naze.getEmail() );
        }
        System.out.println ("\n");
        
        
        // Test d'obtention des dates et coûts d'un pays
        List <DateEtCouts> dec = antarctique.getDec();
        ListIterator it57 = dec.listIterator();
                
        while (it57.hasNext())
        {
            DateEtCouts naze = (DateEtCouts) it57.next();
            System.out.println (naze.getId() + ", " + naze.getDateSejour() + ", " + naze.getLieu() + ", " + naze.getPrix() + ", " + naze.getTransport() + ", " + naze.getCode() );
        }
        
        
        // Test d'obtention des voyages d'un pays
        List <Voyage> voy = Service.rechercherVoyagePays(france.getID());
        ListIterator it34 = voy.listIterator();
        
        while (it34.hasNext())
        {
            Voyage naze = (Voyage) it34.next();
            System.out.println (naze.getId() + ", " + naze.getNom() + ", " + naze.getDuree() + ", " + naze.getDescription() + ", " + naze.getCode() + ", " + naze.isSejour() + ", "  + naze.getPays().getNom() );
        }
        
        // Test d'obtention des circuits d'un pays
        List <Voyage> sej = Service.rechercherVoyagePays(france.getID(), false);
        ListIterator it22 = sej.listIterator();
        
        while (it22.hasNext())
        {
            Voyage naze = (Voyage) it22.next();
            System.out.println (naze.getId() + ", " + naze.getNom() + ", " + naze.getDuree() + ", " + naze.getDescription() + ", " + naze.getCode() + ", " + naze.isSejour() + ", "  + naze.getPays().getNom() );
        }
        
        // Test d'édition de quelques objets métiers
        pigeon.setName ("Helix");
        pigeon.setSurname ("Fossil");
        pigeon.setPassword ("FLAREON");
        Service.editClient (pigeon);
        Client tnguyen1 = Service.rechercherClient (pigeon.getEmail());
        System.out.println (tnguyen1.getId() + ", " + tnguyen1.getGender() + ", " + tnguyen1.getName() + ", " + tnguyen1.getSurname() + ", " +  tnguyen1.getBirthDate() + ", " + tnguyen1.getAddress() + ", " + tnguyen1.getEmail() + ", " + tnguyen1.getTelephone() + ", " + tnguyen1.getAccept() + ", " +  tnguyen1.getPassword());
        
        
        // Affichage des infos d'un pays
        System.out.println ("\n" + Service.genererInfos(france));
        
        // Affichage des infos de quelques voyages
        System.out.println ("\n\n" + Service.genererInfos (japon) + "\n\n");
        System.out.println (Service.genererInfos (antarctique));
        
        // Affichage de mail envoyé à des partenaires commerciaux
        System.out.println ("\n\n" + Service.genererAlerte(nintendo, rapace) + "\n");
        System.out.println ("\n" + Service.genererAlerte(fraise, voleur) + "\n");
        
        
        // Test sur la connexion
        System.out.println (Service.connexion("tuan-hoang.nguyen@insa-lyon.fr", "coturnetropfortalepee"));
        System.out.println (Service.connexion("karim.rien@b3430.3if", "mauvais mot de passe"));
        System.out.println (Service.connexion("je.n.existe@pas", "ana"));
        
        // Test d'obtention d'un pays avec son code
        Pays inconnu = Service.rechercherPaysByCode("CODE");
        System.out.println (inconnu.getNom() + "\n");
    }*/
}